<%--
    Document   : ListaFilmes
    Created on : 20/03/2017, 02:05:54
    Author     : Maxwell de Assis
--%>

<%@page import="controller.ListaFilmesServlet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Filmes</title>
    </head>
    <body>
        <h1>Filmes</h1>
        <div class="container">
            <form method="get" action="<%= request.getContextPath()%>/ListaFilmesServlet">

                <div class="form-group">
                    <label for="IdFilme">
                        ID Filme
                    </label>
                    <input id="IdFilme" name="IdFilme" class="form-control" type="text" placeholder="Digite o ID do Filme">
                </div>

                <div>
                    <input type="submit" value="Buscar" class="btn btn-primary">
                </div>
                <br>
                <table border="2">
                    <c:forEach items="${lista}" var="lst">
                        <tr>
                            <td>
                                <c:out value="${lst.idFilme}"></c:out>
                                </td>
                                <td>
                                <c:out value="${lst.nomeFilme}"></c:out>
                                </td>
                            </tr>
                    </c:forEach>
                </table>
            </form>
        </div>
    </body>
    <!-- Última versão CSS compilada e minificada -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Tema opcional -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Última versão JavaScript compilada e minificada -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>
