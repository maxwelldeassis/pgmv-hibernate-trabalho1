package dao;

import bean.Filme;
import java.io.Serializable;

/**
 *
 * @author Maxwell de Assis
 */
public class FilmeDAO extends GenericDAOImplement<Filme, Serializable> {

}
