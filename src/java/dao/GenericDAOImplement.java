package dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ThreadLocalSessionContext;
import org.hibernate.criterion.Projections;
import util.HibernateUtil;

/**
 *
 * @author Maxwell de Assis
 * @param <T>
 * @param <E>
 */
public class GenericDAOImplement<T, E extends Serializable> implements GenericDAOInterface<T, E> {

    private final Class<T> classe;
    protected Session session;
    protected Transaction tx;

    @SuppressWarnings("unchecked")

    public GenericDAOImplement() {
        this.classe = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.session = HibernateUtil.getSessionFactory().openSession();
        ThreadLocalSessionContext.bind(session);
    }

    @Override
    public E create(T entity) {
        E id;
        List<T> list;
        try {
            startOperation();
            id = (E) session.save(entity);
            tx.commit();
        } catch (HibernateException e) {
            id = null;
            tx.rollback();
            throw e;
        }
        return id;
    }

    @Override
    public void delete(T entity) {
        try {
            startOperation();
            session.delete(entity);
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(T entity) {
        try {
            startOperation();
            session.merge(entity);
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            throw e;
        }
    }

    @Override
    public T findById(Serializable id) {
        T obj;
        try {
            startOperation();
            obj = (T) session.load(classe, id);
            tx.commit();
        } catch (HibernateException e) {
            obj = null;
            tx.rollback();
            throw e;
        }
        return obj;
    }

    @Override
    public List<T> listAll() {
        List<T> list;
        try {
            startOperation();
            list = session.createCriteria(classe).list();
            tx.commit();
        } catch (HibernateException e) {
            list = null;
            tx.rollback();
            throw e;
        }
        return list;
    }

    @Override
    public List<T> findList(int pageNumber, int pageSize) {
        List<T> list;
        try {
            startOperation();
            list = session.createCriteria(classe)
                    .setFirstResult((pageNumber - 1) * pageSize)
                    .setMaxResults(pageSize)
                    .list();
            tx.commit();
        } catch (HibernateException e) {
            list = null;
            tx.rollback();
            throw e;
        }
        return list;
    }

    @Override
    public int getCountAll() {
        Long count;
        try {
            startOperation();
            count = (Long) HibernateUtil.getSession()
                    .createCriteria(classe)
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            count = null;
            tx.rollback();
            throw e;
        }

        if (null == count) {
            return 0;
        } else {
            return count.intValue();
        }
    }

    protected void startOperation() throws HibernateException {
        session = HibernateUtil.getSession();
        tx = session.beginTransaction();
    }

}
