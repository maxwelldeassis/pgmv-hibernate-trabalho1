package dao;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Amanda Maxwell de Assis
 * @param <T>
 * @param <E>
 */
public interface GenericDAOInterface<T, E extends Serializable> {

    E create(T entity);

    void delete(T entity);

    void update(T entity);

    T findById(Serializable id);

    List<T> listAll();

    List<T> findList(int pageNumber, int pageSize);

    int getCountAll();
}
