package bean;
// Generated 14/03/2017 19:51:56 by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * AtorFilmeId generated by hbm2java
 */
@Embeddable
public class AtorFilmeId  implements java.io.Serializable {


     private int idFilme;
     private int idAtor;

    public AtorFilmeId() {
    }

    public AtorFilmeId(int idFilme, int idAtor) {
       this.idFilme = idFilme;
       this.idAtor = idAtor;
    }
   


    @Column(name="id_filme", nullable=false)
    public int getIdFilme() {
        return this.idFilme;
    }
    
    public void setIdFilme(int idFilme) {
        this.idFilme = idFilme;
    }


    @Column(name="id_ator", nullable=false)
    public int getIdAtor() {
        return this.idAtor;
    }
    
    public void setIdAtor(int idAtor) {
        this.idAtor = idAtor;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof AtorFilmeId) ) return false;
		 AtorFilmeId castOther = ( AtorFilmeId ) other; 
         
		 return (this.getIdFilme()==castOther.getIdFilme())
 && (this.getIdAtor()==castOther.getIdAtor());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getIdFilme();
         result = 37 * result + this.getIdAtor();
         return result;
   }   


}


